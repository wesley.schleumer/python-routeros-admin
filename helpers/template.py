from flask import render_template

def t(template, **args):
  return render_template(template + ".jinja.html", **args)
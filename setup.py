from setuptools import setup

setup(name='python-routeros-admin',
      version='0.1',
      description='ok',
      url='https://bitbucket.org/schleumer/python-routeros-admin',
      author='Wesley Schleumer',
      author_email='wesley.schleumer@gmail.com',
      license='MIT',
      install_requires=[
          'markdown',
          'flask',
          'jinja2'
      ],
      zip_safe=False)